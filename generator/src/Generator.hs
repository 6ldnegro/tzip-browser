-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Generator
  ( generate
  ) where

import Universum

import Data.Aeson (encode)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Data.Map as M
import Streamly (asyncly)
import System.Directory (createDirectoryIfMissing)
import Shelly (cp_r, rm_rf, shelly)
import System.FilePath ((</>), (<.>))
import qualified Streamly.Prelude as S
import Streamly.Internal.FileSystem.Dir as S

import Common
import Tzip

-- | Given a source repo path and a destination
-- path, write the index proposal file and data
-- files for each available proposal.
generate :: FilePath -> FilePath -> Text -> IO ()
generate fp target repoUrl = do
  -- In the Tzip repo, proposals are in the "proposals" directory. So
  -- we set to read the files from the same.
  let srcDir = fp </> "proposals"

  createDirectoryIfMissing True proposalsDir

  tzipMap <- S.foldlM' (foldFn srcDir) mempty . asyncly
    $ S.toDirs srcDir
    & S.map (getMarkdownPath srcDir)
    & S.mapM (\fp' -> (fp', ) <$> BS.readFile fp')
    & S.filter (\(_, b) -> BS.length b > 0)
    & S.mapM (try @_ @ProcessingException . parseProposal repoUrl)
    & S.mapMaybeM (\case
        Right p -> pure $ Just p
        Left err -> do
          logMessage $ show err
          pure $ Nothing) -- @TODO Log this somewhere
    & S.mapM compileProposal

  writeProposals (target </> "proposals" <.> "json") tzipMap repoUrl
  where
    proposalsDir = target </> "proposal"

    proposalToFilePath :: Tzip -> FilePath
    proposalToFilePath Tzip{tzProposalId} =
      proposalsDir </> toString tzProposalId <.> "json"

    foldFn :: FilePath -> TzipBubbles -> (Tzip, TzipBubble) -> IO TzipBubbles
    foldFn srcDir m (tzip, tzbubble) = do
        BSL.writeFile (proposalToFilePath tzip) $ encode tzip
        shelly $ do
          let srcPath = srcDir </> toString (tzProposalId tzip)
              destPath = proposalsDir </> toString (tzProposalId tzip)
          rm_rf destPath
          cp_r srcPath destPath
        pure $ M.insert (tzProposalId tzip) tzbubble m

    getMarkdownPath :: FilePath -> FilePath -> FilePath
    getMarkdownPath proposals tzip = proposals </> tzip </> (tzip <.> "md")

-- | Write the index proposals.json which contains metadata of all available
-- proposals
writeProposals :: FilePath -> TzipBubbles -> Text -> IO ()
writeProposals fp metas repoUrl = BSL.writeFile fp $ encode $ IndexProposals metas Nothing repoUrl
