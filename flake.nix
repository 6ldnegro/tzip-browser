# SPDX-FileCopyrightText: 2021 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later

{
  nixConfig = {
    flake-registry = "https://github.com/serokell/flake-registry/raw/master/flake-registry.json";
  };

  inputs = {
    flake-compat.flake = false;
    haskell-nix = {
      inputs.hackage.follows = "hackage";
      inputs.stackage.follows = "stackage";
    };
    hackage.flake = false;
    stackage.flake = false;
  };

  outputs = { self, nixpkgs, haskell-nix, flake-utils, hackage, stackage
    , nix-npm-buildpackage, deploy-rs, serokell-nix, flake-compat }:
    (flake-utils.lib.eachDefaultSystem (system:
      let
        inherit (serokell-nix.lib) pkgsWith;

        pkgs = pkgsWith nixpkgs.legacyPackages.${system} [
          haskell-nix.overlay
          nix-npm-buildpackage.overlay
          serokell-nix.overlay
        ];

        backend = import ./generator { inherit pkgs; };
        frontend = import ./browser { inherit pkgs; };

      in rec {
        packages.ci-shell = pkgs.mkShell {
          buildInputs = [ deploy-rs.defaultPackage.${system} pkgs.nixUnstable ];
        };

        packages.tzip-generator = backend.components.exes.tzip-generator;
        packages.tzip-listener = backend.components.exes.tzip-listener;
        packages.tzip-browser = frontend;

        defaultPackage = pkgs.symlinkJoin {
          name = "tzip-web";
          paths = [ packages.tzip-browser packages.tzip-generator ];
        };

        checks = {
          generator-test = backend.checks.tzip-generator-test;
          whitespace = pkgs.build.checkTrailingWhitespace ./.;
          reuse = pkgs.runCommand "reuse-lint" { buildInputs = [ pkgs.reuse ]; } ''
            cd ${./.}
            reuse lint
            touch $out
          '';
        } // deploy-rs.lib.${system}.deployChecks self.deploy;
      })) // {
        deploy = let
          mkNode = hostname: {
            inherit hostname;
            user = "deploy";
            profiles = {
              tzip-web-listener.path =
                deploy-rs.lib.x86_64-linux.activate.custom
                self.packages.x86_64-linux.tzip-listener
                "sudo /run/current-system/sw/bin/systemctl restart tzip-web-listener";
              tzip-web-browser.path = deploy-rs.lib.x86_64-linux.activate.noop
                self.packages.x86_64-linux.tzip-browser;
            };
          };
        in {
          sshOpts = [ "-p 17788" ];
          nodes = {
            staging = mkNode "agora.tezos.serokell.team";
            production = mkNode "www.tezosagora.org";
          };
        };

        overlay = final: prev: {
          tzip-generator = self.packages.${final.system}.tzip-generator;
          tzip-browser = self.packages.${final.system}.tzip-browser;
        };

        nixosModules = {
          tzip-web-backend =
            import ./nix/modules/services/backend.nix { inherit self; };
          tzip-web-frontend =
            import ./nix/modules/services/frontend.nix { inherit self; };
        };

        nixosConfigurations.container = nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            self.nixosModules.tzip-web-backend
            self.nixosModules.tzip-web-frontend

            ({ config, pkgs, lib, ... }: {
              system.configurationRevision = lib.mkIf (self ? rev) self.rev;
              boot.isContainer = true;
              networking.useDHCP = false;
              networking.firewall.allowedTCPPorts = [ 80 ];
              networking.hostName = "tzip-web";

              services.tzip-web-listener = {
                secretFile = builtins.toFile "gitlab-secret" "TZIP_LISTENER_GITLAB_TOKEN=testgitlabtoken";
                nginxVhost = "tzip-web";
              };
              services.tzip-web-browser = {
                dataPath = config.services.tzip-web-listener.outputPath;
                nginxVhost = "tzip-web";
              };

              services.nginx = {
                enable = true;
                recommendedProxySettings = true;
                virtualHosts = {
                  "tzip-web" = {
                    default = true;
                    serverName = "tzip-web";
                    forceSSL = false;
                    # The rest is configured by the tzip-web-browser module.
                  };
                };
              };
            })
          ];
        };
      };
}
