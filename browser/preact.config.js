// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { resolve } from "path";
import * as webpack from "webpack";

function trimSlashes(str) {
    while (str.startsWith("/")) {
        str = str.slice(1);
    }

    while (str.endsWith("/")) {
        str = str.slice(0, -1);
    }

    return str;
}

export default {
    /**
     * Function that mutates the original webpack config.
     * Supports asynchronous changes when a promise is returned (or it's an async function).
     *
     * @param {object} config - original webpack config.
     * @param {object} env - options passed to the CLI.
     * @param {WebpackConfigHelpers} helpers - object with useful helpers for working with the webpack config.
     * @param {object} options - this is mainly relevant for plugins (will always be empty in the config), default to an empty object
     **/
    webpack(config, env) {
        config.module.rules[4].use.splice(1, 0, {
            loader: "@teamsupercell/typings-for-css-modules-loader",
            options: {
                banner:
                    "// This file is automatically generated from your CSS. Any edits will be overwritten.",
                disableLocalsExport: true
            }
        });

        // Use any `index` file, not just index.js
        config.resolve.alias["preact-cli-entrypoint"] = resolve(
            process.cwd(),
            "src",
            "index"
        );

        // For react compatibility
        config.resolve.alias["react"] = "preact/compat";
        config.resolve.alias["react-dom/test-utils"] = "preact/test-util";
        config.resolve.alias["react-dom"] = "preact/compat";

        let urlPrefix = "";
        if (typeof env["env.urlPrefix"] === "string") {
            // ^ this value could be a boolean (if no value is provided) or
            // a number (if empty string is provided) to the --env.urlPrefix argument.
            const trimmed = trimSlashes(env["env.urlPrefix"]);
            if (trimmed.length == 0) {
                urlPrefix = "/";
            } else {
                urlPrefix = "/" + trimmed + "/";
            }
        } else {
            urlPrefix = "/";
        }
        console.log(
            "Using urlPrefix: " + urlPrefix + ". Use --env.urlPrefix to change."
        );

        config.output.publicPath = urlPrefix;

        if (typeof env["env.discourseUrl"] === "string") {
            const discourseUrl = env["env.discourseUrl"];
            console.log("Using discourseUrl: " + discourseUrl);
            config.plugins.push(
                new webpack.DefinePlugin({
                    "process.env.discourseUrl": JSON.stringify(discourseUrl)
                })
            );
        } else {
            console.log(
                "Discourse Url was not provided. Use --env.discourseUrl to specify."
            );
        }
    }
};
