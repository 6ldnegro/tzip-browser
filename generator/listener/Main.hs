-- SPDX-FileCopyrightText: 2021 Tezos Commons
-- SPDX-License-Identifier: AGPL-3.0-or-later

module Main
  ( main
  ) where

import Universum

import qualified Network.Socket as NS
import Network.Wai.Handler.Warp (defaultSettings, run, runSettingsSocket,
                                 setPort)
import qualified Shelly as S
import System.Environment (lookupEnv)

import Common
import Middlewares.Error (customErrorMiddleware)
import Server (Config(..), regeneratingFiles, server)

main :: IO ()
main = do
  configE <- parseEnvConfig
  case configE of
    Right config -> do
      S.shelly (checkSourceDir config)
      regeneratingFiles config
      case (cIsListenOnUnixSocket config) of
        True -> do
          logMessage $ "Gitlab Listener binding to UNIX sockets on port: " <> show (cPort config)
          sock <- NS.socket NS.AF_UNIX NS.Stream 0
          NS.bind sock $ NS.SockAddrUnix "warp.sock"
          NS.listen sock NS.maxListenQueue
          -- | Run the server
          let settings = setPort (cPort config) defaultSettings
          runSettingsSocket settings sock . customErrorMiddleware $ server config
          -- | Cleanup: Close socket
          NS.close sock
          pure ()
        False -> do
          logMessage $ "Gitlab Listener running on port: " <> show (cPort config)
          run (cPort config) . customErrorMiddleware $ server config
    Left err -> do
      logMessage "Fail to parse ENV variable for Config."
      logMessage $ "  - " <> err

parseEnvConfig :: IO (Either Text Config)
parseEnvConfig = do
  gitlabTokenE <- lookupEnv_ "TZIP_LISTENER_GITLAB_TOKEN"
  outputDirE <- lookupEnv_ "TZIP_LISTENER_OUTPUT_DIR"
  sourceDirE <- lookupEnv_ "TZIP_LISTENER_SOURCE_DIR"

  portE <- lookupEnvAndRead @Int "TZIP_LISTENER_PORT"
  isListenOnUnixSocketE <- lookupEnvAndRead @Bool "TZIP_LISTENER_LISTEN_UNIX_SOCKET"
  repoLinkE <- lookupEnv_ "TZIP_LISTENER_REPO_LINK"

  let (result :: Either Text Config) = do
        gitlabToken <- gitlabTokenE
        outputDir <- outputDirE
        sourceDir <- sourceDirE
        isListen <- Right $ either (\_ -> False) id isListenOnUnixSocketE
        port <- Right $ either (\_ -> 8081) id portE
        repoLink <- Right $ either (\_ -> "https://gitlab.com/tezos/tzip") id repoLinkE

        pure $ Config gitlabToken outputDir sourceDir repoLink isListen port

  pure result

  where
    lookupEnv_ :: Text -> IO (Either Text Text)
    lookupEnv_ var = do
      valO <- lookupEnv (toString var)
      pure $ case valO of
        Just ""  -> Left $ var <> " can't be empty."
        Just val -> Right $ toText val
        Nothing  ->  Left $ var <> " not found."

    lookupEnvAndRead :: forall a. (Read a) => Text -> IO (Either Text a)
    lookupEnvAndRead var = do
      valO <- lookupEnv_ var
      pure $ case valO of
        Right val -> first (\err -> var <> ": " <> err) $ readEither val
        Left err  -> Left err

checkSourceDir :: Config -> S.Sh ()
checkSourceDir config = do
  S.cd sourceDir
  repoExist <- S.test_d "tzip"
  case repoExist of
    False -> do
      logMessage "'tzip' directory does not exist:"
      logMessage "  - Clone TZIP repository"
      S.mkdir "tzip"; S.cd "tzip"
      S.run_ "git" ["clone", (cRepoLink config) <> ".git", "."]
    True -> pure ()
  where
    sourceDir = toString $ cSourceDir config
