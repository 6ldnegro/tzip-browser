// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import { configure } from "enzyme";
import Adapter from "enzyme-adapter-preact-pure";

configure({
    adapter: new Adapter()
});
