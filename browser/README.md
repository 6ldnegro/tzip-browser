<!--
SPDX-FileCopyrightText: 2020 Tocqueville Group

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# tzip-browser

## CLI Commands
*   `npm install`: Installs dependencies

*   `npm run dev`: Run a development, HMR server

*   `npm run serve`: Run a production-like server

*   `npm run build`: Production-ready build

*   `npm run lint`: Pass TypeScript files using TSLint

*   `npm run test`: Run Jest and Enzyme with
    [`enzyme-adapter-preact-pure`](https://github.com/preactjs/enzyme-adapter-preact-pure) for
    your tests


For detailed explanation on how things work, checkout the [CLI Readme](https://github.com/developit/preact-cli/blob/master/README.md).


## Serving the data files during development

When you run `yarn dev` it starts the frontend, but the forntend requires the
generated data files in order for it to display something.
Here is an easy wat to get them:

* Go to the `generator` directory in the root of the repository.
* Run `stack build` to build the binary, it will be called `tzip-generator`.
* Run `stack exec -- tzip-generator --path <path/to/data> --target ../browser/src/static`

This will put the generated files into the `src/static` subdirectory of this
directory, where the development web-server will find them and serve correctly.

* Now simply start `yarn dev`.
