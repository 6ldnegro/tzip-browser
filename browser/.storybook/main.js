// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const path = require('path');

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    path.resolve('./.storybook/webpack-final'),
    "@storybook/addon-links",
    "@storybook/addon-essentials",
  ]
}