// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { useEffect } from "preact/hooks";
import { Tzip } from "../../common";
import RenderedContainer from "../../components/rendered_container";
import { Link } from "preact-router/match";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Helmet from "react-helmet";
import { faCode } from "@fortawesome/free-solid-svg-icons/faCode";

export interface Props {
    publicPath: string;
    // We need proposal id propoerty here so that we can receive the
    // proposal id from the route.
    tzip: Tzip;
    discourseUrl?: string;
}

function mkPageTitle(tzip: Tzip) {
    return tzip.proposalId.toUpperCase() + ": " + tzip.meta.title;
}

const Proposal: FunctionalComponent<Props> = (props: Props) => {
    useEffect(() => {
        if (
            props.discourseUrl !== undefined &&
            props.tzip?.meta?.discourseTopicId
        ) {
            (window as any).DiscourseEmbed = {
                discourseUrl: props.discourseUrl, // "http://localhost:3000/",
                topicId: props.tzip.meta.discourseTopicId,
                discourseReferrerPolicy: "strict-origin-when-cross-origin"
            };

            (function() {
                const d = document.createElement("script");
                d.type = "text/javascript";
                d.async = true;
                d.src =
                    (window as any).DiscourseEmbed.discourseUrl +
                    "javascripts/embed.js";
                (
                    document.getElementsByTagName("head")[0] ||
                    document.getElementsByTagName("body")[0]
                ).appendChild(d);
            })();
        }
    }, [props.discourseUrl, props.tzip.meta.discourseTopicId]);

    return (
        <div>
            <Helmet>
                {/* This thing sets the document title. */}
                <title>{mkPageTitle(props.tzip)}</title>
            </Helmet>
            <Link href={props.publicPath} class="back-button">
                <svg width="18" height="18">
                    <g
                        fill="none"
                        fillRule="evenodd"
                        stroke="#FFF"
                        strokeWidth="2"
                    >
                        <path strokeLinejoin="round" d="M2 9h16"></path>
                        <path d="M10 17L2 9l8-8"></path>
                    </g>
                </svg>
            </Link>
            <div class="proposal-id">
                <div class="item">{props.tzip.proposalId}</div>
                <div class="direct-link">
                    <a
                        rel="noreferrer"
                        target="_blank"
                        href={props.tzip.directLink}
                    >
                        <FontAwesomeIcon icon={faCode} color="#444" />
                    </a>
                </div>
            </div>
            <div class="proposal-title">{props.tzip?.meta?.title}</div>
            <div class="proposal-attribute">
                Status: {props.tzip?.meta?.status}
            </div>
            <div class="proposal-attribute">
                Author: {props.tzip?.meta?.author}
            </div>
            <div class="proposal-attribute">
                Created: {props.tzip?.meta?.created}
            </div>
            <RenderedContainer
                class="proposal-container markdown-body"
                innerHtml={props.tzip?.rendered}
            />
            <div class="discourse-container" id="discourse-comments"></div>
        </div>
    );
};

export default Proposal;
