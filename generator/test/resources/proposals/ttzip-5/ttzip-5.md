---
SPDX-FileCopyrightText: 2020 Tocqueville Group
SPDX-License-Identifier: AGPL-3.0-or-later
tzip: 5
title: TTZIPs – test --- TZIPs (copy)
status: Draft
type: Meta
author: Kirill Elagin
created: 2020-09-06
---

## Summary

This is a TZIP-like document meant to use for _testing purposes_.
It doesn’t containt any useful information, just some structure that resembles
that of actual TZIPs.

## Abstract

Tezos is a blockchain platform and, since it is meant to be used by a large
number of people, it needs standards. A [TZIP][TZIP-1] is a Tezos standard.
There is a [whole repository][tzips] of such standards. It is possible that
tools will be created for working with these standards (for example,
presenting them in a nice way), and therefore it is useful to have test data
to feed to those tools for testing. This file is one such example.

## Rationale

First of all, TZIPs are Markdown files that contain information. But they are
not just any Markdown files – TZIPs have to be structured in a special way,
which makes them more uniform and easier to navigate and use. [TZIP-1] is
a TZIP that specified the structure of all TZIPs.

Humans like developing tools that make their lives easier. In particular,
it is easy to imagine someone creating a tool that will process TZIPs in
some way, for example, to make navigation easier – it can allow one to
sort and filter TZIPs based on their metadata or it can allow full-text
search in their titles and bodies.

Such a tool would need to be tested. It is often convenient to have small test
inputs, rather than using actual real data, which tends to be bigger than needed
for most test cases. Hence TTZIPs – a collection of test data that resembles
TZIPs, but is not too large in size.

## TTZIP workflow

Just add new files next to this one. Feel free to fill the files with all
kinds of syntax and weird things, so that they can be used in test cases.

## Some raw html

<table>
  <tr>
    <th>RPC schema</th>
    <th>Request schema</th>
  </tr>
  <tr>
    <td>
      <pre>{
    "kind": "transaction",
    "amount": $mutez,
    "destination": $contract_id,
    "source": $contract_id,
    "counter": $positive_bignum,
    "fee": $mutez,
    "gas_limit": $positive_bignum,
    "storage_limit": $positive_bignum,
    "parameters"?: $micheline.michelson_v1.expression
}</pre>
    </td>
    <td>
      <pre>{
    "kind": "transaction",
    "amount": $mutez,
    "destination": $contract_id,
    "fee"?: $mutez,
    "gas_limit"?: $positive_bignum,
    "storage_limit"?: $positive_bignum,
    "parameters"?: $micheline.michelson_v1.expression
}</pre>
    </td>
  </tr>
</table>


[TZIP-1]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-1/tzip-1.md
[tzips]: https://gitlab.com/tzip/tzip/
