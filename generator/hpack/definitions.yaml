# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-License-Identifier: AGPL-3.0-or-later

# This file defines sensible defaults but does not actually apply
# any of them, so it can be safely included.
# The only exception is `cabal-version`.
# `hpack` deduces 2.0 and causes `cabal check` to fail for `indigo`
# and potentially for other packages.

verbatim:
  cabal-version: 2.2

_definitions:
  _top-level:
    - &meta
        maintainer:          Serokell <hi@serokell.io>
        license:             AGPL-3.0-or-later
        license-file:        LICENSE
        git:                 git@gitlab.com:tezosagora/tzip-browser.git
        homepage:            https://gitlab.com/tezosagora/tzip-browser
        bug-reports:         https://gitlab.com/tezosagora/tzip-browser/-/issues

    - &default-extensions
        - AllowAmbiguousTypes
        - ApplicativeDo
        - BangPatterns
        - BlockArguments
        - ConstraintKinds
        - DataKinds
        - DefaultSignatures
        - DeriveAnyClass
        - DeriveDataTypeable
        - NoImplicitPrelude
        - DeriveFoldable
        - DeriveFunctor
        - DeriveGeneric
        - DeriveTraversable
        - DerivingStrategies
        - DerivingVia
        - EmptyCase
        - FlexibleContexts
        - FlexibleInstances
        - GADTs
        - GeneralizedNewtypeDeriving
        - LambdaCase
        - MultiParamTypeClasses
        - MultiWayIf
        - NamedFieldPuns
        - NegativeLiterals
        - NumDecimals
        - OverloadedLabels
        - OverloadedStrings
        - PatternSynonyms
        - PolyKinds
        - QuasiQuotes
        - RankNTypes
        - RecordWildCards
        - RecursiveDo
        - ScopedTypeVariables
        - StandaloneDeriving
        - StrictData
        - TemplateHaskell
        - TupleSections
        - TypeApplications
        - TypeFamilies
        - TypeOperators
        - UndecidableInstances
        - UndecidableSuperClasses
        - ViewPatterns

    - &dependencies
        - name: base
          version: ">= 4.7 && < 5"
          mixin: [hiding (Prelude)]

    - &ghc-options
        - -Weverything
        - -Wno-missing-exported-signatures
        - -Wno-missing-import-lists
        - -Wno-missed-specialisations
        - -Wno-all-missed-specialisations
        - -Wno-unsafe
        - -Wno-safe
        - -Wno-missing-local-signatures
        - -Wno-monomorphism-restriction
        - -Wno-implicit-prelude

  _utils:
    # Additional options, they will be merged with the ones above when both
    # are imported.

    - &lib-common
        source-dirs: src

    - &exec-common
        main:        Main.hs
        source-dirs: app

    - &test-common
        main:        Main.hs
        source-dirs: test
        build-tools: tasty-discover:tasty-discover

        ghc-options:
          - -threaded
          - -eventlog
          - -rtsopts
            # Weird quoting is required for cabal to correctly pass this as _one_ option,
            # otherwise it splits by spaces.
            # :facepalm:
          - '"-with-rtsopts=-N -A64m -AL256m"'

    - &bench-common
        main:        Main.hs
        source-dirs: bench
