---
SPDX-FileCopyrightText: 2020 Tocqueville Group
SPDX-License-Identifier: AGPL-3.0-or-later
tzip: 4
title: TTZIPs – test --- TZIPs (copy)
status: Draft
type: Meta
author: Kirill Elagin
created: 2020-09-06
---

## Summary

This is a TZIP-like document meant to use for _testing purposes_.
It doesn’t containt any useful information, just some structure that resembles
that of actual TZIPs.

## Abstract

Tezos is a blockchain platform and, since it is meant to be used by a large
number of people, it needs standards. A [TZIP][TZIP-1] is a Tezos standard.
There is a [whole repository][tzips] of such standards. It is possible that
tools will be created for working with these standards (for example,
presenting them in a nice way), and therefore it is useful to have test data
to feed to those tools for testing. This file is one such example.

## Rationale

First of all, TZIPs are Markdown files that contain information. But they are
not just any Markdown files – TZIPs have to be structured in a special way,
which makes them more uniform and easier to navigate and use. [TZIP-1] is
a TZIP that specified the structure of all TZIPs.

Humans like developing tools that make their lives easier. In particular,
it is easy to imagine someone creating a tool that will process TZIPs in
some way, for example, to make navigation easier – it can allow one to
sort and filter TZIPs based on their metadata or it can allow full-text
search in their titles and bodies.

Such a tool would need to be tested. It is often convenient to have small test
inputs, rather than using actual real data, which tends to be bigger than needed
for most test cases. Hence TTZIPs – a collection of test data that resembles
TZIPs, but is not too large in size.

## TTZIP workflow

Just add new files next to this one. Feel free to fill the files with all
kinds of syntax and weird things, so that they can be used in test cases.

## A Table

| Status             | Actor    | Description                                                                                                                            | Action(s) to get that Status                                      | Next Status(es)         |
| :----------------- | :------- | :------------------------------------------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------- | :---------------------- |
| `Work In Progress` | Author   | This covers the initial work being done to create the TZIP and its accompanying resources                                              | 1. Author submits a MR to:<br/>&nbsp;&nbsp;- reserve a TZIP number, say *xx*, in the [README](/README.md#current-tzips)<br/>&nbsp;&nbsp;- add the *Work In Progress* status in the README<br/>&nbsp;&nbsp;- create a *tzip-xx* folder in [proposals](/proposals)<br/>2. Author creates a new branch, *tzip-xx*, to work on it      | Draft, Withdrawn |
| `Draft`            | Author   | The TZIP is now in good shape, and is ready to be shared with the community to get iterative feedback                                  | 1. Author submits a MR from the *tzip-xx* branch, including the status change to *Draft* in the README<br/>2. Reviewer merges the MR to *master* | Submitted, Withdrawn |
| `Withdrawn`        | -        | The authors decide that the TZIP is no longer needed, or the reviewers decide to reject it (e.g. for non-compliance)                   | 1. Author or Reviewer closes the *tzip-xx* branch<br/>2. Author or Reviewer updates the TZIP and README with the *Withdrawn* status                       | -                    |
| `Submitted`        | Reviewer | The TZIP has been approved by the community and is submitted to the reviewers                                                          | Author updates the TZIP and README with the *Submitted* status    | Final, Draft, Withdrawn |
| `Final`            | Reviewer | The TZIP has been approved by the reviewers                                                                                            | Reviewer updates the TZIP and README with the *Final* status      | Deprecated, Superseded  |
| `Deprecated`       | -        | The TZIP is no longer valid and shouldn't be used anymore. It is kept here as a reference but hasn't been superseded by any newer TZIP | Reviewer updates the TZIP and README with the *Deprecated* status | -                       |
| `Superseded`       | -        | The TZIP is no longer valid, but unlike the *Deprecated* status, this TZIP has been superseded by a newer TZIP                         | Reviewer updates the TZIP and README with the *Superseded* status | -                       |


[TZIP-1]: https://gitlab.com/tzip/tzip/-/blob/master/proposals/tzip-1/tzip-1.md
[tzips]: https://gitlab.com/tzip/tzip/
