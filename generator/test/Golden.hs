-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Golden
  ( test_Proposals
  ) where

import Universum

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import System.FilePath (takeBaseName, (</>), (<.>))

import Test.Tasty (TestTree)
import Test.Tasty.Golden (findByExtension, goldenVsString)

import Tzip
import Common


test_Proposals :: IO [TestTree]
test_Proposals =
    findByExtension [".md"] testProposalsPath <&> map testOneProposal
  where
    testProposalsPath :: FilePath
    testProposalsPath = "test/resources/proposals/"

    goldenFilesPath :: FilePath
    goldenFilesPath = "test/resources/golden/"

    testOneProposal :: FilePath -> TestTree
    testOneProposal fp =
        goldenVsString ("Testing " <> fp) (mkGoldenPath fp) (getGeneratedHtml fp)
      where
        mkGoldenPath :: FilePath -> FilePath
        mkGoldenPath fp_ = goldenFilesPath </> (takeBaseName fp_) <.> "golden"

        getGeneratedHtml :: FilePath -> IO BSL.ByteString
        getGeneratedHtml fp_ = do
          (tzip, _) <- ((fp, ) <$> BS.readFile fp_)
            >>= parseProposal "http://repourl.com" >>= compileProposal
          pure $ encodeUtf8 $ getSafeHtml $ tzRendered tzip
