// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

import React from "react";

declare global {
    namespace React {
        interface ReactElement {
            /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
            nodeName: any;
            /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
            attributes: any;
            /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
            children: any;
        }
    }
}
