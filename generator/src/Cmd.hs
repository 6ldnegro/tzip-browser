-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Cmd
  ( Command (..)
  ) where

import Universum

import Options.Generic (ParseRecord, type (<?>))

data Command =
  GenerateCommand
    { path :: FilePath <?> "Path to the checked out TZIP repo"
    , target :: FilePath <?> "Path to the destination directory"
    , repoUrl :: Maybe Text <?> "Link to the actual proposal repo"
    }
  deriving stock (Generic, Show)

instance ParseRecord Command
