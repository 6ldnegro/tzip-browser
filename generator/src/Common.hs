-- SPDX-FileCopyrightText: 2020 Tocqueville Group
-- SPDX-License-Identifier: AGPL-3.0-or-later
module Common
  ( CustomDerivation
  , SafeHtml
  , doesTocExist
  , getSafeHtml
  , logMessage
  , mkSafeHtml
  , renderDoc
  , renderDocWithToc
  ) where

import Universum

import Data.Aeson (FromJSON, ToJSON)
import Data.Default (def)
import qualified Data.Text as T
import Deriving.Aeson (CamelTo, CustomJSON(..), FieldLabelModifier, StripPrefix)
import Text.HTML.SanitizeXSS (sanitize)
import Text.Pandoc (Block(..), Inline(..), Pandoc(..), compileTemplate)
import Text.Pandoc.Class (PandocIO)
import Text.Pandoc.Options (WriterOptions(..))
import Text.Pandoc.Writers.HTML (writeHtml5String)

type CustomDerivation pfx t = CustomJSON '[FieldLabelModifier (StripPrefix pfx, CamelTo "-")] t

-- | A function that write to stderr. This is because in server stuff
-- written to stderr has a better chance of ending up in the log.
logMessage :: MonadIO m => Text -> m ()
logMessage = hPutStrLn stderr

-- This type mirrors the `SafeHtml` type in frontend that hold sanitized
-- html we can safely interpolate into the frontend html. The JSON instances
-- for this type should not be changed.
newtype SafeHtml = SafeHtml { __html :: Text }
  deriving stock (Generic, Eq, Ord, Show)
  deriving (FromJSON, ToJSON) via CustomDerivation "" SafeHtml

getSafeHtml :: SafeHtml -> Text
getSafeHtml = __html

mkSafeHtml :: Text -> SafeHtml
mkSafeHtml = SafeHtml . sanitize

renderDoc :: Pandoc -> PandocIO SafeHtml
renderDoc doc = mkSafeHtml <$> writeHtml5String def doc

renderDocWithToc :: Pandoc -> PandocIO SafeHtml
renderDocWithToc doc =
  let
    tocTemplate = runIdentity $ compileTemplate [] "<h2>Table of contents</h2>\n\n$toc$\n$body$"
    opt = def
      { writerTableOfContents = True
      , writerTOCDepth = 3
      , writerTemplate = rightToMaybe tocTemplate
      }
  in mkSafeHtml <$> writeHtml5String opt doc

doesTocExist :: Pandoc -> Bool
doesTocExist (Pandoc _ blks) =
  let
    unwordsInline :: [Inline] -> Text
    unwordsInline is = is
      & fmap (\case
          Str s -> Just s
          _ -> Nothing
        )
      & catMaybes
      & unwords

    headers = blks
        & fmap (\case
                  Header _ _ i -> Just $ T.toLower $ unwordsInline i
                  _ -> Nothing )
        & catMaybes

  in "table of contents" `elem` headers
