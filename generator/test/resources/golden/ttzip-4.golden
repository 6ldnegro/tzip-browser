<h2>Table of contents</h2>

<ul>
<li><a href="#summary">Summary</a></li>
<li><a href="#abstract">Abstract</a></li>
<li><a href="#rationale">Rationale</a></li>
<li><a href="#ttzip-workflow">TTZIP workflow</a></li>
<li><a href="#a-table">A Table</a></li>
</ul>
<h2 id="summary">Summary</h2>
<p>This is a TZIP-like document meant to use for <em>testing purposes</em>. It doesn’t containt any useful information, just some structure that resembles that of actual TZIPs.</p>
<h2 id="abstract">Abstract</h2>
<p>Tezos is a blockchain platform and, since it is meant to be used by a large number of people, it needs standards. A <a href="../tzip-1/">TZIP</a> is a Tezos standard. There is a <a href="https://gitlab.com/tzip/tzip/">whole repository</a> of such standards. It is possible that tools will be created for working with these standards (for example, presenting them in a nice way), and therefore it is useful to have test data to feed to those tools for testing. This file is one such example.</p>
<h2 id="rationale">Rationale</h2>
<p>First of all, TZIPs are Markdown files that contain information. But they are not just any Markdown files – TZIPs have to be structured in a special way, which makes them more uniform and easier to navigate and use. <a href="../tzip-1/">TZIP-1</a> is a TZIP that specified the structure of all TZIPs.</p>
<p>Humans like developing tools that make their lives easier. In particular, it is easy to imagine someone creating a tool that will process TZIPs in some way, for example, to make navigation easier – it can allow one to sort and filter TZIPs based on their metadata or it can allow full-text search in their titles and bodies.</p>
<p>Such a tool would need to be tested. It is often convenient to have small test inputs, rather than using actual real data, which tends to be bigger than needed for most test cases. Hence TTZIPs – a collection of test data that resembles TZIPs, but is not too large in size.</p>
<h2 id="ttzip-workflow">TTZIP workflow</h2>
<p>Just add new files next to this one. Feel free to fill the files with all kinds of syntax and weird things, so that they can be used in test cases.</p>
<h2 id="a-table">A Table</h2>
<table>
<thead>
<tr class="header">
<th style="text-align:left">Status</th>
<th style="text-align:left">Actor</th>
<th style="text-align:left">Description</th>
<th style="text-align:left">Action(s) to get that Status</th>
<th style="text-align:left">Next Status(es)</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align:left"><code>Work In Progress</code></td>
<td style="text-align:left">Author</td>
<td style="text-align:left">This covers the initial work being done to create the TZIP and its accompanying resources</td>
<td style="text-align:left">1. Author submits a MR to:<br />  - reserve a TZIP number, say <em>xx</em>, in the <a href="/README.md#current-tzips">README</a><br />  - add the <em>Work In Progress</em> status in the README<br />  - create a <em>tzip-xx</em> folder in <a href="/proposals">proposals</a><br />2. Author creates a new branch, <em>tzip-xx</em>, to work on it</td>
<td style="text-align:left">Draft, Withdrawn</td>
</tr>
<tr class="even">
<td style="text-align:left"><code>Draft</code></td>
<td style="text-align:left">Author</td>
<td style="text-align:left">The TZIP is now in good shape, and is ready to be shared with the community to get iterative feedback</td>
<td style="text-align:left">1. Author submits a MR from the <em>tzip-xx</em> branch, including the status change to <em>Draft</em> in the README<br />2. Reviewer merges the MR to <em>master</em></td>
<td style="text-align:left">Submitted, Withdrawn</td>
</tr>
<tr class="odd">
<td style="text-align:left"><code>Withdrawn</code></td>
<td style="text-align:left">-</td>
<td style="text-align:left">The authors decide that the TZIP is no longer needed, or the reviewers decide to reject it (e.g. for non-compliance)</td>
<td style="text-align:left">1. Author or Reviewer closes the <em>tzip-xx</em> branch<br />2. Author or Reviewer updates the TZIP and README with the <em>Withdrawn</em> status</td>
<td style="text-align:left">-</td>
</tr>
<tr class="even">
<td style="text-align:left"><code>Submitted</code></td>
<td style="text-align:left">Reviewer</td>
<td style="text-align:left">The TZIP has been approved by the community and is submitted to the reviewers</td>
<td style="text-align:left">Author updates the TZIP and README with the <em>Submitted</em> status</td>
<td style="text-align:left">Final, Draft, Withdrawn</td>
</tr>
<tr class="odd">
<td style="text-align:left"><code>Final</code></td>
<td style="text-align:left">Reviewer</td>
<td style="text-align:left">The TZIP has been approved by the reviewers</td>
<td style="text-align:left">Reviewer updates the TZIP and README with the <em>Final</em> status</td>
<td style="text-align:left">Deprecated, Superseded</td>
</tr>
<tr class="even">
<td style="text-align:left"><code>Deprecated</code></td>
<td style="text-align:left">-</td>
<td style="text-align:left">The TZIP is no longer valid and shouldn&#39;t be used anymore. It is kept here as a reference but hasn&#39;t been superseded by any newer TZIP</td>
<td style="text-align:left">Reviewer updates the TZIP and README with the <em>Deprecated</em> status</td>
<td style="text-align:left">-</td>
</tr>
<tr class="odd">
<td style="text-align:left"><code>Superseded</code></td>
<td style="text-align:left">-</td>
<td style="text-align:left">The TZIP is no longer valid, but unlike the <em>Deprecated</em> status, this TZIP has been superseded by a newer TZIP</td>
<td style="text-align:left">Reviewer updates the TZIP and README with the <em>Superseded</em> status</td>
<td style="text-align:left">-</td>
</tr>
</tbody>
</table>