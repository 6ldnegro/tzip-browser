# SPDX-FileCopyrightText: 2021 Tezos Commons
# SPDX-License-Identifier: AGPL-3.0-or-later

{ pkgs }:

let
  projectSrc = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "tzip-generator";
    src = ../.;
    subDir = "generator";
  };

  # haskell.nix does not support 'include' in package.yaml, we have to generate .cabal ourselves
  cabalFile = pkgs.runCommand "tzip-generator.cabal" {} ''
    ${pkgs.haskellPackages.hpack}/bin/hpack ${projectSrc} - > $out
  '';

  project = pkgs.haskell-nix.stackProject {
    # project src with .cabal file added
    src = pkgs.runCommand "src-with-cabal" {} ''
      cp -r --no-preserve=mode ${projectSrc} $out
      cp ${cabalFile} $out/tzip-generator.cabal
    '';
    ignorePackageYaml = true;
    modules = [{
      packages.tzip-generator = {
        # strip executable to reduce closure size
        dontStrip = false;
      };
    }];
  };

in project.tzip-generator
