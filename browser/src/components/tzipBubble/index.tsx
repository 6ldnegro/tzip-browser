// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { route } from "preact-router";
import RenderedContainer from "../rendered_container";
import { TzipBubble } from "../../common";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCode } from "@fortawesome/free-solid-svg-icons/faCode";

export interface Props {
    publicPath: string;
    tzipId: string;
    tzipBubble: TzipBubble;
    repoUrl: string;
}

function convertStatus(st: string | undefined): string | undefined {
    if (st && st == "Work In Progress") return "WIP";
    else return st;
}

function getStatusColor(st: string | undefined): string {
    switch (convertStatus(st)) {
        case "Final": {
            return "status-final";
        }
        case "Deprecated": {
            return "status-deprecated";
        }
        case "WIP": {
            return "status-wip";
        }
        default: {
            return "status-default";
        }
    }
}

export const TzipBubbleComp: FunctionalComponent<Props> = (props: Props) => {
    return (
        <div class="tzip-container">
            <div
                class="tzip-clickable"
                onClick={(): void => {
                    route(`${props.publicPath}proposal/${props.tzipId}/`);
                }}
            >
                <div class="tzip-id">{props.tzipId}</div>
                <div class="tzip-title">{props.tzipBubble.meta.title}</div>
                <RenderedContainer
                    class="tzip-intro"
                    innerHtml={props.tzipBubble.summary}
                />
            </div>
            <div class="tzip-status-and-link">
                <div
                    class={
                        "tzip-status " +
                        getStatusColor(props.tzipBubble.meta?.status)
                    }
                >
                    {convertStatus(props.tzipBubble.meta.status)}
                </div>

                <div class="tzip-repo-link">
                    <a
                        rel="noreferrer"
                        target="_blank"
                        href={props.tzipBubble.directLink}
                    >
                        <FontAwesomeIcon icon={faCode} color="#444" />
                    </a>
                </div>
            </div>
        </div>
    );
};

export default TzipBubbleComp;
