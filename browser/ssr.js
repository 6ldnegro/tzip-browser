// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/* eslint-disable */
const { h } = require('preact');
const { basename } = require('path');
const { readFileSync } = require('fs');
const compression = require('compression')();
const render = require('preact-render-to-string');
const bundle = require('./build/ssr-build/ssr-bundle');
const fs = require('fs');

var preact = require('preact');
var undom = require('undom');

// Some code that reads the generated proposal data and pass it to the
// root app during ssr.
function readFileAsJSON(filename) {
  return JSON.parse(fs.readFileSync(filename, 'utf8'));
}

function readProposalData() {
  let proposalsFolder = "src/static";

  let proposalsData = readFileAsJSON(proposalsFolder + "/proposals.json");

  let allProposals = {};

  let fullProposals = proposalsFolder + "/proposal/"
  const files = fs.readdirSync(fullProposals);

  files.forEach(f => {
    let filePath = fullProposals + f;
    if (fs.lstatSync(filePath).isFile()) {
      let propData = readFileAsJSON(filePath);
      allProposals[propData["proposal-id"]] = propData;
    }
  });

  return { index : proposalsData, proposals : allProposals };
}
//
const App = bundle.default;

// set up a rendering target
var renderer = createRenderer();

// Render path provided via the cmd line argument
renderer.render(h(App, { path: process.argv[2], publicPath: "/", tzipData : readProposalData() }));

// Wait for a bit and render the html
setTimeout(function() {
  console.log(renderer.html()); }, 500);

// Some infra to do dom rendering
var doc;
function createRenderer() {
  if (!doc) {
    doc = undom();
    Object.assign(global, doc.defaultView);
  }

  var parent = doc.createElement('div');
  var root = parent;

  doc.body.appendChild(parent);

  return {
    render: function(jsx) {
      preact.render(jsx, parent, root);
      return this;
    },
    html: function() {
      return serialize(root.childNodes[0]);
    }
  };
}

function serialize(el) {
  if (el.nodeType===3) return el.textContent;
  var name = String(el.nodeName).toLowerCase(),
    str = '<'+name,
    c, i;
  for (i=0; i<el.attributes.length; i++) {
    str += ' '+el.attributes[i].name+'="'+el.attributes[i].value+'"';
  }
  str += '>';
  for (i=0; i<el.childNodes.length; i++) {
    c = serialize(el.childNodes[i]);
    if (c) str += '\n\t'+c.replace(/\n/g,'\n\t');
  }
  return str + (c?'\n':'') + '</'+name+'>';
}

function enc(s) {
  return s.replace(/[&'"<>]/g, function(a){ return `&#${a};` });
}
