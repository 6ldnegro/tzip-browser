// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

/** @jsx h */
import { FunctionalComponent, h } from "preact";
import { TzipBubbles, FilterSortOptions } from "../../common";
import TzipBubbleComp from "../../components/tzipBubble";

interface Props {
    repoUrl: string;
    publicPath: string;
    selector: FilterSortOptions;
    tzips: TzipBubbles;
}

const Tzips: FunctionalComponent<Props> = (props: Props) => {
    const tzipComps: Array<h.JSX.Element> = [];

    const filteredSortedTzip: TzipBubbles = props.selector.filterSort(
        props.tzips
    );

    filteredSortedTzip.forEach((val, key) => {
        tzipComps.push(
            <TzipBubbleComp
                publicPath={props.publicPath}
                repoUrl={props.repoUrl}
                tzipBubble={val}
                tzipId={key}
            />
        );
    });

    return <div class="tzips-container">{tzipComps}</div>;
};

export default Tzips;
