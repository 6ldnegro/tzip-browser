# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-License-Identifier: AGPL-3.0-or-later

{ self }:

{ config, lib, pkgs, ... }:

let
  tzipPkgs = self.packages.${config.nixpkgs.system};
  cfg = config.services.tzip-web-browser;

in {

  options = {
    services.tzip-web-browser = {
      dataPath = lib.mkOption {
        type = lib.types.path;
        description = ''
          Path with the generated data files.
        '';
      };

      package = lib.mkOption {
        type = lib.types.path;
        default = tzipPkgs.tzip-browser;
      };

      nginxVhost = lib.mkOption {
        type = lib.types.str;
        description = ''
          Name of the nginx virtual host to configure.
        '';
      };
    };
  };

  config = {
    services.nginx.virtualHosts.${cfg.nginxVhost} = {
      locations = {
        "/" = {
          root = cfg.dataPath;
          tryFiles = ''$uri @frontend'';
        };
        "@frontend" = {
          root = cfg.package;
          tryFiles = "$uri $uri/ /index.html";
        };
      };
    };
  };

}
