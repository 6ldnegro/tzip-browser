// SPDX-FileCopyrightText: 2020 Tocqueville Group
//
// SPDX-License-Identifier: AGPL-3.0-or-later

declare const styles: {
    readonly notfound: string;
};
export = styles;
