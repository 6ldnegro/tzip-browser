{- This is an example Dhall configuration file
SPDX-FileCopyrightText: 2020 Tocqueville Group
SPDX-License-Identifier: AGPL-3.0-or-later
-}
{ roots = [ "^Main.main$", "Tree.main", "^Paths_.*" ], type-class-roots = True }
