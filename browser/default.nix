# SPDX-FileCopyrightText: 2020 Tocqueville Group
# SPDX-License-Identifier: AGPL-3.0-or-later

{ pkgs, urlPrefix ? "/", discourseUrl ?  "https://forum.tezosagora.org/" }:

pkgs.buildYarnPackage {
  src = ./.;

  doCheck = false;  # TODO: tests fail
  checkPhase = ''
    yarn lint
    yarn test
  '';

  yarnPackPhase = ''
    yarn build --env.urlPrefix="${urlPrefix}" --env.discourseUrl="${discourseUrl}"
  '';

  installPhase = ''
    mv build $out
  '';
}
